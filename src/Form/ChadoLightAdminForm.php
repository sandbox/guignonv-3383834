<?php

namespace Drupal\chadol\Form;

use Drupal\Component\Utility\DeprecationHelper;
use Drupal\Core\Database\Database;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\ByteSizeMarkup;
use Drupal\Core\Url;

/**
 * Chado Light Admin form.
 */
class ChadoLightAdminForm extends FormBase {

  /**
   * Current Chado Light config.
   *
   * @var Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * Config factory.
   *
   * @var Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Entity type manager.
   *
   * @var Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * A PostgreSQL cross-database tool.
   *
   * @var Drupal\dbxschema_pgsql\Database\DatabaseTool
   */
  protected $dbTool;

  /**
   * Create method for factories.
   */
  public static function create($container) {
    $form = parent::create($container);
    $form->configFactory = $container->get('config.factory');
    $form->config = $form->configFactory->getEditable('chadol.settings');
    $form->entityTypeManager = $container->get('entity_type.manager');
    $form->dbTool = $container->get('dbxschema.tool');
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'chadol_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['#attached']['library'][] = 'chadol/global-styling';
    // Chado instances.
    $form['chado_instances'] = [
      '#type'  => 'details',
      '#title' => $this->t('Chado Instances'),
      '#open'  => TRUE,
      '#tree'  => FALSE,
    ];
    // List detected Chado instances.
    $form['chado_instances']['table'] = [
      '#type' => 'table',
      '#caption' => $this->t('List of Chado instances'),
      '#header' => [
        $this->t('Db key'),
        $this->t('Schema'),
        $this->t('Version'),
        $this->t('Data'),
        $this->t('Builtin content types'),
      ],
    ];
    $chado_dbkeys = [];
    // Get registered databases in settings.php.
    $database_info = Database::getAllConnectionInfo();
    foreach ($database_info as $dbkey => $targets) {
      if (array_key_exists('default', $targets)) {
        // Check if we got a Chado database.
        if ('pgsql' == $targets['default']['driver']) {
          $connection = $this->dbTool->getConnection('', $dbkey);
          $chado_instances = chadol_get_available_instances($connection);
          foreach ($chado_instances as $chado_instance) {
            $form['chado_instances']['table'][] = [
              [
                '#type' => 'markup',
                '#markup' => $dbkey,
              ],
              [
                '#type' => 'markup',
                '#markup' => $chado_instance['schema_name'],
              ],
              [
                '#type' => 'markup',
                '#markup' => $chado_instance['version'],
              ],
              [
                '#type' => 'markup',
                '#markup' => ($chado_instance['has_data']
                  ? $this->t(
                    'Yes (@size)',
                    [
                      '@size' => DeprecationHelper::backwardsCompatibleCall(
                        \Drupal::VERSION,
                        '10.2.0',
                        fn() => ByteSizeMarkup::create($chado_instance['size']),
                        fn() => DeprecationHelper::backwardsCompatibleCall(\Drupal::VERSION, '10.2.0', fn() => ByteSizeMarkup::create($chado_instance['size']), fn() => format_size($chado_instance['size']))
                      ),
                    ]
                  )
                  : $this->t('No')
                ),
              ],
              $this->getBuiltinForm($dbkey, $chado_instance['schema_name']),
            ];
            $chado_dbkeys[$dbkey] = $chado_dbkeys[$dbkey] ?? [];
            $chado_dbkeys[$dbkey][$chado_instance['schema_name']] = TRUE;
          }
        }
      }
    }

    // Chado content.
    $form['chado_contents'] = [
      '#type'  => 'details',
      '#title' => $this->t('Chado Contents'),
      '#open'  => TRUE,
      '#tree'  => FALSE,
    ];
    // @todo List known Chado content types and detect others.
    $form['chado_contents']['table'] = [
      '#type'  => 'table',
      '#caption' => $this->t('Chado Content types'),
      '#header' => [
        $this->t('Content type'),
        '',
        $this->t('Db key'),
        $this->t('Schema'),
        $this->t('Actions'),
      ],
    ];
    // Process all xntt types.
    $xntt_type_storage = $this->entityTypeManager->getStorage('external_entity_type');
    $entity_types = $xntt_type_storage->getQuery()->accessCheck(TRUE)->execute();
    foreach ($entity_types as $entity_type_name) {
      $entity_type = $xntt_type_storage->load($entity_type_name);
      $base_type = '';
      $is_chado_content_type = FALSE;
      $db_key = '';
      $chado_schemas = [];
      // Check if it is using xnttchado or xnttdb.
      if ('xnttchado' == $entity_type->getStorageClientId()) {
        $is_chado_content_type = TRUE;
        $base_type = 'xnttchado';
        $cl_config = $entity_type->getStorageClientConfig();
        $db_key = $cl_config['connection']['dbkey'];
        $chado_schemas = $cl_config['connection']['schemas'];
      }
      elseif ('xnttdb' == $entity_type->getStorageClientId()) {
        // Check if it is using a Chado schema.
        $cl_config = $entity_type->getStorageClientConfig();
        $base_type = 'xnttdb';
        if (array_key_exists($cl_config['connection']['dbkey'], $chado_dbkeys)) {
          $chado_schemas = array_intersect(
            array_keys($chado_dbkeys[$cl_config['connection']['dbkey']]),
            $cl_config['connection']['schemas']
          );
          if (!empty($chado_schemas)) {
            $is_chado_content_type = TRUE;
            $db_key = $cl_config['connection']['dbkey'];
          }
        }
      }
      if ($is_chado_content_type) {
        $schemas = array_shift($chado_schemas);
        if (!empty($chado_schemas)) {
          $schemas .= ' (' . implode(', ', $chado_schemas) . ')';
        }
        $form['chado_contents']['table'][] = [
          [
            '#type' => 'link',
            '#title' => $entity_type->getLabel(),
            '#url' => Url::fromRoute('entity.external_entity_type.' . $entity_type_name . '.edit_form'),
          ],
          [
            '#type' => 'markup',
            '#markup' => $base_type,
          ],
          [
            '#type' => 'markup',
            '#markup' => $db_key,
          ],
          [
            '#type' => 'markup',
            '#markup' => $schemas,
          ],
          [
            '#type' => 'link',
            '#title' => $this->t('List'),
            '#url' => Url::fromRoute('entity.' . $entity_type_name . '.collection'),
          ],
        ];
      }
    }
    // Add a dropdown and a button to include new contents.
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config->save();
    $this->messenger()->addMessage($this->t('Chado Light settings have been updated.'));
  }

  /**
   * Returns the list of builtin type.
   *
   * @return array
   *   Keys are table name and values are array of info.
   */
  protected function getBuiltinTypes() {
    return [
      'db' => [
        'singular' => $this->t('Source Database'),
        'plural'   => $this->t('Source Databases'),
        'short'    => $this->t('DB'),
      ],
      'dbxref' => [
        'singular' => $this->t('Cross-reference'),
        'plural'   => $this->t('Cross-references'),
        'short'    => $this->t('DBXRef'),
      ],
      'cv' => [
        'singular' => $this->t('Controlled Vocabulary'),
        'plural'   => $this->t('Controlled Vocabularies'),
        'short'    => $this->t('CV'),
      ],
      'cvterm' => [
        'singular' => $this->t('Term'),
        'plural'   => $this->t('Terms'),
        'short'    => $this->t('CVTerm'),
      ],
      'pub' => [
        'singular' => $this->t('Publication'),
        'plural'   => $this->t('Publications'),
        'short'    => $this->t('Pub'),
      ],
      'organism' => [
        'singular' => $this->t('Organism'),
        'plural'   => $this->t('Organisms'),
        'short'    => $this->t('Orga.'),
      ],
    ];
  }

  /**
   * Returns status or button to create builtin contents.
   */
  protected function getBuiltinForm($dbkey, $schema) {
    $builtin_types = $this->getBuiltinTypes();
    $form = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['chadol-builtin-contents'],
      ],
    ];
    $xntt_type_storage = $this->entityTypeManager->getStorage('external_entity_type');

    foreach ($builtin_types as $builtin_type => $type_info) {
      $type_id = chadol_get_builtin_type_id($builtin_type, $dbkey, $schema);
      $entity_type = $xntt_type_storage->load($type_id);
      if (empty($entity_type)) {
        $form[$type_id . '_create'] = [
          '#type' => 'submit',
          '#name' => $type_id,
          '#value' => $this->t(
            '+ @type_label',
            ['@type_label' => $type_info['short']]
          ),
          '#attributes' => [
            'title' => $this->t(
              'Create @label builtin content type',
              ['@label' => $type_info['singular']]
            ),
          ],
        ];
      }
      else {
        $form[$type_id . '_link'] = [
          '#type' => 'link',
          '#title' => $type_info['short'],
          '#url' => Url::fromRoute('entity.external_entity_type.' . $type_id . '.edit_form'),
          '#attributes' => [
            'title' => $type_info['singular'],
          ],
        ];
      }
    }

    // Check if builtin content types have been already created.
    return $form;
  }

}
