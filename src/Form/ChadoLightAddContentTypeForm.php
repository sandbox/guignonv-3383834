<?php

namespace Drupal\chadol\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Chado Light Admin form.
 */
class ChadoLightAddContentTypeForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'chadol_add_content_type_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Chado Content Name'),
      '#maxlength' => 255,
      '#default_value' => '',
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#title' => $this->t('Machine name'),
      '#default_value' => '',
      '#machine_name' => [
        'exists' => [$this, 'exists'],
        'replace_pattern' => '([^a-z0-9_\-\.]+)',
        'error' => 'The machine-readable name must be unique, and can only contain lowercase letters, numbers, underscores, dashes and dots.',
      ],
      '#description' => $this->t('The machine-readable name must be unique, and can only contain lowercase letters, numbers, underscores, dashes and dots.'),
    ];

    $form['create'] = [
      '#type' => 'submit',
      '#value' => $this->t('Create and configure'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // @todo assert the content type name is available.
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // @todo Create the external entity type with simple field mapper and chado
    // storage and redirect to that external entity storage edit page
    // "#edit-storage".
    // $form_state->setRedirect(...);
  }

}
