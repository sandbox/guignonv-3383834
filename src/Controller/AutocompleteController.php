<?php

namespace Drupal\chadol\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Database\Database;
use Drupal\Core\Database\DatabaseExceptionWrapper;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AutocompleteController handles form input autocompletion.
 */
class AutocompleteController {

  /**
   * Autocomplete response page for multiple types.
   *
   * This autocomplete method reads the user input from the query string "q".
   * It supports autocompletion for ($type):
   * - 'cvterm': autocomplete a Chado cvterm from its name.
   *   It can be restricted by parameters:
   *   - 'cv-' followed by a cv_id value: restrict terms to a given CV.
   *   - 'descendent-' followed by a cvterm_id value: restrict terms to
   *     descendents of a given term (in a tree graph of term relationships).
   *   - 'table-type_id-' followed by a table name: restrict terms to the
   *     corresponding 'type_id's of a given table (that must have a 'type_id'
   *     column).
   *   - 'table-cvterm_id-' followed by a table name: restrict terms to the one
   *     used in the 'cvterm_id' column of a given table (that must have a
   *     'cvterm_id' column).
   * - 'cvterms': same as 'cvterm' but happen the suggestion to a coma-separated
   *   list of cvterms.
   * - 'phylonode': autocompletes phylonode names.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object that contains the query string with the word to
   *   complete.
   * @param string $dbkey
   *   The Chado database key or 'default' if using Drupal database.
   * @param string $schema
   *   The Chado schema name to use.
   * @param string $type
   *   The type of data to complete.
   * @param string $params
   *   Some parameters to adjust or filter completion. See method description.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   A JSON response object with the matching words as an array of associative
   *   arrays with 'value' and 'label' keys.
   */
  public function handleAutocomplete(
    Request $request,
    string $dbkey = '',
    string $schema = '',
    string $type = '',
    string $params = ''
  ) {
    $max_suggestions = 20;
    $results = [];

    // Check input.
    $input = $request->query->get('q');
    if (!$input) {
      return new JsonResponse($results);
    }
    $input = Xss::filter($input);

    // Check database parameters.
    $database_info = Database::getAllConnectionInfo();
    if (!array_key_exists($dbkey, $database_info)
        || ('pgsql' != $database_info[$dbkey]['default']['driver'])
    ) {
      // Invalid database.
      return new JsonResponse($results);
    }
    $dbtool = \Drupal::service('dbxschema.tool');
    if ($dbtool->isInvalidSchemaName($schema)) {
      // Invalid schema name.
      return new JsonResponse($results);
    }
    $connection = $dbtool->getConnection($schema, $dbkey);
    $chado_instances = chadol_get_available_instances($connection);
    if (!array_key_exists($schema, $chado_instances)) {
      // Invalid Chado schema.
      return new JsonResponse($results);
    }
    $input = $connection->escapeLike($input);

    $results = [];
    switch ($type) {
      case 'cvterms':
        $terms = explode(',', $input);
        $text_input = array_pop($terms);
        // The rest is common with 'cvterm', no "break;".
      case 'cvterm':
        if (empty($text_input)) {
          $text_input = $input;
        }
        $placeholders = [':value' => $text_input . '%'];
        if ('' == $params) {
          // Any term.
          $query = "
            SELECT cvt.cvterm_id, cvt.name || ' (' || cv.name || ')' AS \"name\"
            FROM {1:cvterm} cvt JOIN {1:cv} cv USING (cv_id)
            WHERE cvt.name ILIKE :value
            ORDER BY 2, 1
            LIMIT $max_suggestions
          ";
        }
        elseif (preg_match('/(?:^|\W)cv-(\d+)/', $params, $matches)) {
          // Limit terms to a given CV.
          $query = "
            SELECT cvt.cvterm_id, cvt.name
            FROM {1:cvterm} cvt
            WHERE cvt.cv_id = :cv_id AND cvt.name ILIKE :value
            ORDER BY 2, 1
            LIMIT $max_suggestions
          ";
          $placeholders[':cv_id'] = $matches[1];
        }
        elseif (preg_match('/(?:^|\W)children-(\d+)/', $params, $matches)) {
          // Limit terms to children of a given CV term.
          $query = "
            SELECT cvt.cvterm_id, cvt.name
            FROM {1:cvterm} cvt
              JOIN {1:cvterm_relationship} cvtr ON cvtr.subject_id = cvt.cvterm_id
              JOIN {1:cvterm} cvt2 ON cvt2.cvterm_id = cvtr.object_id AND cvt2.cv_id = cvt.cv_id
              JOIN {1:cvterm} cvt3 ON cvt3.cvterm_id = cvtr.type_id AND cvt3.cv_id = cvt.cv_id
            WHERE
              cvt2.cvterm_id = :cvterm_id
              AND cvt3.name = 'is_a'
              AND cvt.name ILIKE :value
            ORDER BY 2, 1
            LIMIT $max_suggestions;
          ";
          $placeholders[':cvterm_id'] = $matches[1];
        }
        elseif (preg_match('/(?:^|\W)descendent-(\d+)/', $params, $matches)) {
          // Limit terms to descendent of a given CV term.
          $query = "
            WITH RECURSIVE subcvt AS (
              SELECT cvt.*
              FROM {1:cvterm} cvt
                JOIN {1:cvterm_relationship} cvtr ON cvtr.subject_id = cvt.cvterm_id
                JOIN {1:cvterm} cvt2 ON cvt2.cvterm_id = cvtr.object_id AND cvt2.cv_id = cvt.cv_id
                JOIN {1:cvterm} cvt3 ON cvt3.cvterm_id = cvtr.type_id AND cvt3.cv_id = cvt.cv_id
              WHERE cvt2.cvterm_id = :cvterm_id AND cvt3.name = 'is_a'
              UNION ALL
              SELECT cvt.*
              FROM {1:cvterm} cvt
                JOIN {1:cvterm_relationship} cvtr ON cvtr.subject_id = cvt.cvterm_id
                JOIN subcvt cvt2 ON cvt2.cvterm_id = cvtr.object_id AND cvt2.cv_id = cvt.cv_id
                JOIN {1:cvterm} cvt3 ON cvt3.cvterm_id = cvtr.type_id AND cvt3.cv_id = cvt.cv_id
              WHERE cvt3.name = 'is_a'
            )
            SELECT subcvt.cvterm_id, subcvt.name
            FROM subcvt subcvt
            WHERE subcvt.name ILIKE :value
            ORDER BY 2, 1
            LIMIT $max_suggestions;
          ";
          $placeholders[':cvterm_id'] = $matches[1];
        }
        elseif (preg_match('/(?:^|\W)table-type_id-(\w+)/', $params, $matches)) {
          // Limit terms to type_id of a given table.
          $query = "
            SELECT cvt.cvterm_id, cvt.name || ' (' || cv.name || ')' AS \"name\"
            FROM {1:cvterm} cvt
              JOIN {1:" . $matches[1] . "} t ON cvt.cvterm_id = t.type_id
              JOIN {1:cv} cv USING (cv_id)
            WHERE cvt.name ILIKE :value
            GROUP BY 1, 2
            ORDER BY 2, 1
            LIMIT $max_suggestions
          ";
        }
        elseif (preg_match('/(?:^|\W)table-cvterm_id-(\w+)/', $params, $matches)) {
          // Limit terms to type_id of a given table_cvterm.
          $query = "
            SELECT cvt.cvterm_id, cvt.name || ' (' || cv.name || ')' AS \"name\"
            FROM {1:cvterm} cvt
              JOIN {1:" . $matches[1] . "} t ON cvt.cvterm_id = t.cvterm_id
              JOIN {1:cv} cv USING (cv_id)
            WHERE cvt.name ILIKE :value
            GROUP BY 1, 2
            ORDER BY 2, 1
            LIMIT $max_suggestions
          ";
        }
        else {
          // Unrecognized parameter.
          $query = 'SELECT 0 AS "cvterm_id", \'\' AS "name" WHERE FALSE;';
          $placeholders = [];
        }
        try {
          $cvterm_results = $connection
            ->query($query, $placeholders)
            ->fetchAll();
          // If not all matches, try with the word inside and add results.
          if (count($cvterm_results) < $max_suggestions) {
            $placeholders[':value'] = '%' . $text_input . '%';
            $new_limit = $max_suggestions - count($cvterm_results);
            $query = preg_replace('/ LIMIT \d+/', " LIMIT $new_limit", $query);
            $cvterm_results = array_merge(
              $cvterm_results,
              $connection
                ->query($query, $placeholders)
                ->fetchAll()
            );
          }
        }
        catch (DatabaseExceptionWrapper $e) {
          \Drupal::logger('chadol')->error($e);
          $cvterm_results = [];
        }
        foreach ($cvterm_results as $cvterm_result) {
          $value =
            $cvterm_result->name
            . ' [id:'
            . $cvterm_result->cvterm_id
            . ']';
          if (!empty($terms)) {
            $results[] = [
              'value' => implode(
                ',',
                 array_merge($terms, [$value])
              ),
              'label' => $cvterm_result->name,
            ];
          }
          else {
            $results[] = [
              'value' => $value,
              'label' => $cvterm_result->name,
            ];
          }
        }
        // End of cvterm/cvterms autocompletion.
        break;

      case 'phylonode':
        $placeholders = [':value' => $input . '%'];
        try {
          $query = "
            SELECT
              p.phylonode_id, p.label, o.genus, o.species, cvt.name AS \"type\", t.name AS \"tree\"
            FROM {1:phylonode} p
              JOIN {1:phylotree} t USING (phylotree_id)
              JOIN {1:phylonode_organism} po USING (phylonode_id)
              JOIN {1:organism} o USING (organism_id)
              JOIN {1:cvterm} cvt ON cvt.cvterm_id = p.type_id
            WHERE o.genus || ' ' || o.species ILIKE :value
            ORDER BY 3, 4, 2, 1
            LIMIT $max_suggestions
          ";
          $phylonode_results = $connection
            ->query($query, $placeholders)
            ->fetchAll();
          // If not all matches, try with the word inside and add results.
          if (count($phylonode_results) < $max_suggestions) {
            $placeholders[':value'] = '%' . $input . '%';
            $new_limit = $max_suggestions - count($phylonode_results);
            $query = preg_replace('/ LIMIT \d+/', " LIMIT $new_limit", $query);
            $phylonode_results = array_merge(
              $phylonode_results,
              $connection
                ->query($query, $placeholders)
                ->fetchAll()
            );
          }
        }
        catch (DatabaseExceptionWrapper $e) {
          \Drupal::logger('chadol')->error($e);
          $phylonode_results = [];
        }

        foreach ($phylonode_results as $phylonode_result) {
          if (!empty($phylonode_result->tree)) {
            if (!empty($phylonode_result->type)) {
              $details =
                ' (as "'
                . $phylonode_result->type
                . '" in tree "'
                . $phylonode_result->tree
                . '")';
            }
            else {
              $details =
                ' (in tree "'
                . $phylonode_result->tree
                . '")';
            }
          }
          elseif (!empty($phylonode_result->type)) {
            $details =
              ' (as "'
              . $phylonode_result->type
              . '")';
          }
          else {
            $details = '';
          }
          $label =
            $phylonode_result->genus
            . ' '
            . $phylonode_result->species
            . $details;
          $value = $label . ' [id:' . $phylonode_result->phylonode_id . ']';
          $results[] = [
            'value' => $value,
            'label' => $label,
          ];
        }
        // End of phylonode autocompletion.
        break;

      default:
    }

    return new JsonResponse($results);
  }

}
